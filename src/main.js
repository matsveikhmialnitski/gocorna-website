import "./styles.css";
import { getImgUrl } from "./utils/utils";
import CreateButton from "./components/button/button";
import Header from "./components/Header/header";
import createBlock from "./components/textBlock/textBlock";
import HeroSection from "./pages/hero/hero";
import SafeSection from "./pages/safeSection/safeSection";
import ExpertsSection from "./pages/experts/experts";
import Fingertips from "./pages/fingerprints/fingerprints";

const app = document.querySelector("#app"); // div#app

const downloadButton = CreateButton({ mode: "secondary", string: "download" });

const headerContainer = document.createElement("header");
headerContainer.className = "header";
headerContainer.innerHTML = Header();
headerContainer.append(downloadButton);

const heroSection = HeroSection();
const safeSection = SafeSection();
const expertsSection = ExpertsSection();
const fingerTipsSection = Fingertips();

app.append(headerContainer);

app.append(heroSection);
app.append(safeSection);
app.append(expertsSection);
app.append(fingerTipsSection);
