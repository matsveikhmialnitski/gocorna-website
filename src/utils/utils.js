export function getImgUrl(icon) {
    return new URL(`/src/assets/${icon}`, import.meta.url).href;
  }
  