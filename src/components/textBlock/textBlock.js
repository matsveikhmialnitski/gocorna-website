import './style.css';
import heading from "../headingText/heading";
import CreateButton from "../button/button";

function createBlock(props) {
    const textBlock = document.createElement("div")
    textBlock.className = 'text-block'
    const button = CreateButton(props.button);

    const h2 = heading(props.headingInfo)
    const paragraph = document.createElement('p');
    paragraph.className = 'paragraph';

    const paragraphText = props.paragraph.split('. ').join('.<br>');

    paragraph.innerHTML = paragraphText;

    textBlock.append(h2);
    textBlock.append(paragraph);
    textBlock.append(button);

    return textBlock;
}

export default createBlock;