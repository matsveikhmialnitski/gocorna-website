import { getImgUrl } from '../../utils/utils';
import './style.css'
function Header() {

    return `
    <img src=${getImgUrl('Group 2.svg')}></img>
    <nav>
        <ul class="nav-list">
            <li class="nav-item"><a class="nav-link" href="#">HOME</a></li>
            <li class="nav-item"><a class="nav-link" href="#">FEATURES</a></li>
            <li class="nav-item"><a class="nav-link" href="#">SUPPORT</a></li>
            <li class="nav-item"><a class="nav-link" href="#">CONTACT US</a></li>
        </ul>
    </nav>
`
}
export default Header;