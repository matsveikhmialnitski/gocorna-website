import "./style.css";

function heading(props) {
    const heading = document.createElement('h1');
    heading.className = 'heading';
    heading.textContent = props.text;

    const coloredText = document.createElement('span');
    coloredText.textContent = props.coloredText;

    if (props.color === 'red') {
        coloredText.classList.add('red-text');
    } else if (props.color === 'blue') {
        coloredText.classList.add('blue-text');
    }

    heading.append(document.createTextNode(' '));
    heading.append(coloredText);

    return heading;

}

export default heading;