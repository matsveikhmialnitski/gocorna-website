import "./style.css";
function CreateButton(props) {
  const button = document.createElement("button");
  button.className = "newButton";
  button.innerText = props.string.toUpperCase();
  if (props.mode === "primary") {
    button.classList.add("red");
  } else if (props.mode === "secondary") {
    button.classList.add("blue");
  }

  return button;
}
export default CreateButton;
