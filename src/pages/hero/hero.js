import "./style.css";
import { getImgUrl } from '../../utils/utils';
import createBlock from "../../components/textBlock/textBlock";

function HeroSection() {

    const heroSection = document.createElement('section');
    heroSection.className = 'hero'


    const heroContainer = document.createElement('div');
    heroContainer.className = 'hero-container'

    const imgHero = document.createElement('img');
    imgHero.src = getImgUrl('Group 14.svg')

    const imgHero2 = document.createElement('img');
    imgHero2.src = getImgUrl('Group 23.svg')

    const heroTextBlock = createBlock({
        headingInfo: { text: "Take care of yours family's", color: 'blue', coloredText: 'healt' },
        paragraph: 'All in one destination for COVID-19 health queries. Consult 10,000+ health workers about your conerns.',
        button: { string: 'Get started', mode: 'primary' }
    });

    const textBlockBottom = document.createElement('div');
    textBlockBottom.className = 'text-block-bottom'
    const headerP = document.createElement('p');
    headerP.className = 'title';

    headerP.textContent = 'Stay safe with GoCorona';

    const paragraphBottom = document.createElement('p');
    paragraphBottom.className = 'content';
    paragraphBottom.textContent = 'WATCH VIDEO'

    textBlockBottom.append(headerP, paragraphBottom);

    const left = document.createElement('div');
    left.className = 'left-block';

    const bottom = document.createElement('div');
    bottom.className = 'bottom-block';

    bottom.append(imgHero2, textBlockBottom)

    left.append(heroTextBlock, bottom);

    heroContainer.append(left, imgHero);
    heroSection.append(heroContainer)

    return heroSection;

}

export default HeroSection;