import "./style.css";
import { getImgUrl } from '../../utils/utils';
import createBlock from "../../components/textBlock/textBlock";

function SafeSection() {
    const safeSection = document.createElement('section');
    safeSection.className = 'safe';

    const imgSafe = document.createElement('img');
    imgSafe.className = 'img-safe';
    imgSafe.src = getImgUrl('Group 22.svg');


    const safeTextBlock = createBlock({
        headingInfo: { text: "Stay safe with Go", color: 'red', coloredText: 'Corona.' },
        paragraph: '24x7 Support and user friendly mobile platform to bring healthcare to your fingertips. Signup and be a part of the new health culture.',
        button: { string: 'Features', mode: 'primary' }
    });


    safeSection.append(imgSafe, safeTextBlock)

    return safeSection;
}

export default SafeSection;