import "./style.css";
import { getImgUrl } from '../../utils/utils';
import createBlock from "../../components/textBlock/textBlock";

function ExpertsSection() {
    const expertsSection = document.createElement('section');
    expertsSection.className = 'experts';

    const imgSafe = document.createElement('img');
    imgSafe.src = getImgUrl('Group 26.svg');

    const imgSafeMain = document.createElement('img');
    imgSafeMain.src = getImgUrl('Group 28.svg');
    imgSafeMain.className = 'center-image'

    const expertBlock = document.createElement('div');
    expertBlock.className = 'experts-block';

    const expertsTextBlock = createBlock({
        headingInfo: { text: "Talk to", color: 'blue', coloredText: 'experts.' },
        paragraph: '24x7 Support and user friendly mobile platform to bring healthcare to your fingertips. Signup and be a part of the new health culture.',
        button: { string: 'Features', mode: 'primary' }
    });

    expertBlock.append(expertsTextBlock, imgSafe)

    expertsSection.append(imgSafeMain, expertBlock)

    return expertsSection;
}

export default ExpertsSection;