import "./style.css";
import { getImgUrl } from "../../utils/utils";
import createBlock from "../../components/textBlock/textBlock";

function Fingertips() {
  const fingerTips = document.createElement("section");
  fingerTips.className = "finger-tips";

  const textBlock = document.createElement("div");
  textBlock.className = "text";
  const paragraph = document.createElement("p");
  paragraph.className = "paragraph";

  const h2bottom = `<h2 class="header_bottom">
        <span class="red-h2">Healthcare</span> at your Fingertips.
    </h2>
    <p class="paragraph-footer">Bringing premium healthcare features to your fingertips. User friendly mobile platform to<br> bring healthcare to your fingertips. Signup and be a part of the new health culture.</p>
    `;

  const arrayTitles = ["Symptom Checker", "24x7 Medical support", "Conditions"];
  const arrayContent = [
    "Check if you are infected by<br> COVID-19 with our Symptom Checker",
    "Consult with 10,000+ health<br> workers about your concerns.",
    "Bringing premium healthcare<br> features to your fingertips.",
  ];
  const imgUrls = ["Vector42.svg", "Vector43.svg", "Vector45.svg"];
  const squareBlock = document.createElement("div");
  squareBlock.className = "squares-block";

  for (let i = 0; i < 3; i++) {
    const squareDiv = document.createElement("div");
    squareDiv.className = "square";
    const sample = `
            <div class="square-content">
            <img class="img-center" src=${getImgUrl(`${imgUrls[i]}`)}></img>
            <div class="square-text">
                <h2 class="square-title">${arrayTitles[i]}</h2>
                <p class="grey-text">
                ${arrayContent[i]}
                </p>
            </div>
            </div>
        `;
    squareDiv.innerHTML = sample;
    squareBlock.append(squareDiv);
  }

  const logoFooter = document.createElement("div");
  logoFooter.className = "logo-footer";

  const imgApple = document.createElement("img");
  imgApple.className = "img-logo";
  imgApple.src = getImgUrl("image 1.svg");

  const googlePlayLogo = document.createElement("img");
  googlePlayLogo.className = "img-logo";
  googlePlayLogo.src = getImgUrl("image 2.svg");

  const rectangleRed = document.createElement("div");
  rectangleRed.className = "rectangle-red1";

  const rectangleRed2 = document.createElement("div");
  rectangleRed2.className = "rectangle-red2";

  const rectangleRed3 = document.createElement("div");
  rectangleRed3.className = "rectangle-red3";

  const rectangleRed4 = document.createElement("div");
  rectangleRed4.className = "rectangle-red4";

  squareBlock.append(rectangleRed, rectangleRed2, rectangleRed3, rectangleRed4);

  const divBlocks = document.createElement("div");
  divBlocks.className = "div-blocks-content";
  divBlocks.append(squareBlock);

  logoFooter.append(googlePlayLogo, imgApple);

  textBlock.innerHTML = h2bottom;
  textBlock.append(paragraph);

  fingerTips.append(textBlock, divBlocks, logoFooter);

  return fingerTips;
}

export default Fingertips;
